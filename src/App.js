import React, { useState } from 'react';
import Box from './ProviderConsumer/Box';
import { Color } from './ProviderConsumer/ColorProvider';
import logo from './logo.svg';
import './App.css';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
    </div>
  );
}

function AppFn() {
  const [color, setColor] = useState('red');

  function handleColorChange(e) {
    setColor(e.currentTarget.value);
  }

  return (
    <>
        <Color.Provider value={color}>
            <Box />
        </Color.Provider>
        <div>
            <input type='text' defaultValue={color} onChange={handleColorChange}/>
        </div>
    </>
  );
}

export default AppFn;
