import React, { useEffect, useState} from 'react';

export default function AppEffect() {
    const [count, setCount] = useState(0);

    useEffect(() => {
        console.log('Subscribe: ', count);

        const timerId = setInterval(() => {
            setCount(count + 1);
        }, 1000);

        return () => {
            console.log('Unsubscribe');

            clearInterval(timerId);
        };
    }, [count]);

    return (
        <div>
            Hello Effects!

            Hello {count} times!
        </div>
    );
}