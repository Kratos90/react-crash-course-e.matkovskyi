import React from 'react';

class Homework extends React.Component {
    state = {
        isLoading: false,
        data: null,
        isError: false,
        delay: 5000
    }

    sendRequest = () => {
        this.setState({
            isLoading: true,
            data: null,
            isError: false
        });

        const { delay } = this.state;

        new Promise((res, rej) => {
            this.interval = setTimeout(() => {
                if (parseInt(Math.random() * 100) > 25) {
                    res();
                } else {
                    rej();
                }
            }, delay);
        }).then(() => {
            this.setState({
                isLoading: false,
                data: 'my data',
                isError: false
            });
        }).catch(() => {
            this.setState({
                isLoading: false,
                data: null,
                isError: true
            });
        });
    }

    stopRequest = () => {
        if (this.interval) {
            clearTimeout(this.interval);
        }

        this.setState({
            isLoading: false,
            data: null,
            isError: false
        });
    }

    setDelay = (e) => {
        this.setState({
            delay: e.target.value
        });
    }

    render() {
        const { isLoading, data, isError } = this.state;

        return <>
            <div>Delay (ms): <input name='delay' placeholder='0' onChange={this.setDelay}/></div>
            <button disabled={isLoading} onClick={this.sendRequest}>Send request</button>
            <button disabled={!isLoading} onClick={this.stopRequest}>Stop request</button>
            <div>
                {data && <div>Result of request</div>}
                {isError && <div>
                    Error!
                    <button onClick={this.sendRequest}>Reply request</button>
                </div>}
                
            </div>
        </>
    }
}

export default Homework;