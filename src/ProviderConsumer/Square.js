import React, { useContext } from 'react';
import { Color } from './ColorProvider';

function SquareUI(props) {
    return (<div style={{color: props.color}}>Hello world</div>);
}

export default function Square() {
    const color = useContext(Color);

    return <SquareUI color={color}/>
}