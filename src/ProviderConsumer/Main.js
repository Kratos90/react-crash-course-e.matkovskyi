import React from 'react';
import Box from './Box';
import { Color } from './ColorProvider';

export default class Main extends React.Component {
    state = {
        color: 'red'
    };

    handleColorChange = (e) => {
        this.setState({
            color: e.currentTarget.value
        });
    }

    render() {
        const { color } = this.state;

        return (
            <>
                <Color.Provider value={color}>
                    <Box />
                </Color.Provider>
                <div>
                    <input type='text' defaultValue={color} onChange={this.handleColorChange}/>
                </div>
            </>
        );
    }
}