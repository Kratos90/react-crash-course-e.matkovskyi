import React, { Suspense } from 'react';

const Box = React.lazy(() => import('./LazyComponents/Box'));
const Square = React.lazy(() => import('./LazyComponents/Square'));

export default class LazyExample extends React.PureComponent {
    render() {
        return (
            <Suspense fallback={<div>Loading...</div>}>
                <Box />
                <Square />
            </Suspense>
        );
    }
}