import React from 'react';
import ReactDOM from 'react-dom';

export default class Block1 extends React.Component {
    render() {
        return ReactDOM.createPortal(this.props.children, document.getElementById('block1'))
    }
}