import React, { useState, useRef } from 'react';
import Block1 from './Block1';
import Block2 from './Block2';

class App extends React.Component {
    state = {
        text: ''
    };

    constructor() {
        super();

        this.inputRef = React.createRef();
    }

    handleTextChange = (e) => {
        this.setState({
            text: e.currentTarget.value
        });
    }

    handleFocus = () => {
        this.inputRef.current.focus();
    }

    render() {
        const { text } = this.state;

        return (
            <>
                <div>Hello from portal root!</div>

                <Block1>
                    Hello from {text}
                    <button onClick={this.handleFocus}>Focus!</button>
                </Block1>
                <Block2>
                    <input ref={this.inputRef} type='text' defaultValue={text} onChange={this.handleTextChange}/>
                </Block2>
            </>
        );
    }
}

export default function AppFn() {
    const [text, setText] = useState('Block1');
    const inputRef = useRef();

    const handleTextChange = (e) => {
        setText(e.currentTarget.value);
    };

    const handleFocus = () => {
        inputRef.current.focus();
    };

    return (
        <>
            <div>Hello from portal root!</div>

            <Block1>
                Hello from {text}
                <button onClick={handleFocus}>Focus!</button>
            </Block1>
            <Block2>
                <input ref={inputRef} type='text' defaultValue={text} onChange={handleTextChange}/>
            </Block2>
        </>
    );
}